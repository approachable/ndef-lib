
#include "lib_approach_tag.h"
#include <Arduino.h>

void enableTagRF() {
    M24SR_RFConfig(1);
}

void disableTagRF() {
    M24SR_RFConfig(0);
}

void clearMsgWaiting(){
    TagState.msgWaiting_state = 0;
}

bool msgWaiting() {
    return TagState.msgWaiting_state;
}

void on_gpo() {
    TagState.msgWaiting_state = 1;
}
/**
  * @brief  Initializes the Tag and configures the interrupt settings
  * @retval SUCCESS :  Tag successfully initialized
  */
uint16_t approachTagInit ()
{
    uint16_t status = SUCCESS;
    uint16_t gpo_status = SUCCESS;
    uint8_t CCBuffer[15];
    sCCFileInfo *pCCFile;
    
    pCCFile = &CCFileStruct;
    
    pinMode(M24SR_RFDIS_PIN, OUTPUT);
    pinMode(M24SR_GPO_PIN, INPUT);
    
    TagState.msgWaiting_state = 0;

    status = TagT4Init( CCBuffer, sizeof(CCBuffer));
    
    if( status == SUCCESS)
    {   
        pCCFile->NumberCCByte = (uint16_t) ((CCBuffer[0x00]<<8) | CCBuffer[0x01]);
        pCCFile->Version = CCBuffer[0x02];
        pCCFile->MaxReadByte = (uint16_t) ((CCBuffer[0x03]<<8) | CCBuffer[0x04]);
        pCCFile->MaxWriteByte = (uint16_t) ((CCBuffer[0x05]<<8) | CCBuffer[0x06]);
        pCCFile->TField = CCBuffer[0x07];
        pCCFile->LField = CCBuffer[0x08];
        pCCFile->FileID = (uint16_t) ((CCBuffer[0x09]<<8) | CCBuffer[0x0A]);
        pCCFile->NDEFFileMaxSize = (uint16_t) ((CCBuffer[0x0B]<<8) | CCBuffer[0x0C]);
        pCCFile->ReadAccess = CCBuffer[0x0D];
        pCCFile->WriteAccess = CCBuffer[0x0E];  
    }
    
    delay(10);
    
    if(OpenNDEFSession(SYSTEM_FILE_ID, ASK_FOR_SESSION) == SUCCESS){
        gpo_status = M24SR_ManageGPO(HIGH_IMPEDANCE, I2C_GPO);
        gpo_status = M24SR_ManageGPO(I2C_ANSWER_READY, RF_GPO);
        CloseNDEFSession(SYSTEM_FILE_ID);
    }
    
    attachInterrupt(digitalPinToInterrupt(M24SR_GPO_PIN), on_gpo, RISING);
    
    return status;
}   
/**
  * @brief  Reads the Approachable record from the tag and fills in the Tag Structure
    * @param    nfc_buffer : pointer to the buffer where we will put the tag data
    * @param    tagStructure : contains pointers and sizes for the sub-components of the Tag and NDEF Record
  * @retval ATR_SUCCESS :  Tag successfully written
    * @retval ATR_NDEF_FAIL : Some issue with the NDEF, most likely not the right type
    * @retval ATR_MIME_FAIL : MIME Type mismatch
  */
ApproachTagResult approachTagRead(char* nfc_buffer, TagStructure* tagStructure) {
    uint16_t status = ERROR;
    sRecordInfo headerData = {0};
    sRecordInfo *pHeaderData = &headerData;
    uint32_t payloadSize;

    if(OpenNDEFSession(NDEF_FILE_ID, ASK_FOR_SESSION) == SUCCESS){
    status = NDEF_ReadNDEF(nfc_buffer);
    CloseNDEFSession(NDEF_FILE_ID);
    }
    
    tagStructure->tag_buffer = nfc_buffer;
    tagStructure->record = nfc_buffer + 2;
    
    // Serial.println("\nNFCBuff 100 bytes:");
    // Serial.write(nfc_buffer,100);
    // Serial.println("\n");
  
    status = NDEF_IdentifySPRecord ( pHeaderData, (nfc_buffer + 2) );
    payloadSize = ((uint32_t)(pHeaderData->PayloadLength3)<<24) | \
                ((uint32_t)(pHeaderData->PayloadLength2)<<16) | \
                ((uint32_t)(pHeaderData->PayloadLength1)<<8)  | \
                pHeaderData->PayloadLength0;
                
    tagStructure->payload_size = payloadSize;
    tagStructure->payload = (char*)(tagStructure->record + pHeaderData->PayloadOffset);

    //read and validate the type
    if (strlen(MIMETYPE) == pHeaderData->TypeLength) {
        if (!memcmp(MIMETYPE,pHeaderData->Type,pHeaderData->TypeLength)) {
            // Serial.write((char*)(tagStructure->record + pHeaderData->PayloadOffset), tagStructure->payload_size);
            return ATR_SUCCESS;
            //status = process_JSON((char*)(nfc_buffer + 2 + pHeaderData->PayloadOffset), payloadSize);
        } else {
            return ATR_MIME_FAIL;
        }
    } else {
        return ATR_NDEF_FAIL;
    }
}

/**
  * @brief  Converts the payload into and NDEF Record, buffer_size needs to have additional space for NDEF Header
    * @param    nfc_buffer : pointer to the NDEF buffer with the payload starting at beginning (function will shift memory to add header)
    * @param    payload_size : the size of the JSON payload stored in the buffer
    * @param    buffer_size : the total memory available on the buffer
  * @retval ATR_SUCCESS :  Tag successfully written
    * @retval ATR_ALLOC_FAIL : Not enough memory was allocated for payload and header
  */
ApproachTagResult approachTagWrite(uint8_t* nfc_buffer, uint8_t payload_size, int buffer_size) {
    int tag_length; //length of data to be written to the tag
    int ndef_length;
    uint8_t type_length;
    uint8_t header_length;
    uint8_t* nfcptr;
    uint8_t ndef_three_byte[3] = {0xD2, 0xFF, 0xFF};
    int i;
    uint16_t status = ERROR;
    
    /*short record, no ID, 3 bytes + MIME Length + Payload Length
    the length should not exceed 256 byte for this demo. Long record is needed for 
    lengths longer than 256*/
    type_length = strlen(MIMETYPE);
    header_length = type_length + 3;
    ndef_length = payload_size + header_length;
    tag_length = ndef_length + 2;//2  extra bytes for Tag Type 4 size bytes
    if (tag_length > buffer_size || tag_length > 256) {
        return ATR_ALLOC_FAIL;
    }
    
    /*Shift Payload to make room for header*/
    for(i = payload_size; i>=0; i--) {
        nfc_buffer[i+header_length+2] = nfc_buffer[i];
    }
    
    /*Build the Header*/
    ndef_three_byte[1] = type_length;
    ndef_three_byte[2] = payload_size;

    nfcptr = nfc_buffer;
    memset((void*) nfcptr, 0, 1);//zeros first byte
    memcpy(nfcptr+1, &ndef_length, 2);//changes only second byte, good for up to 256 bytes
    nfcptr += 2;
    memcpy(nfcptr, ndef_three_byte, 3);
    nfcptr += 3;
    memcpy(nfcptr, MIMETYPE, type_length);
    nfcptr += type_length;
    
    /*Write the Tag*/
    if(OpenNDEFSession(NDEF_FILE_ID, ASK_FOR_SESSION) == SUCCESS){
        status = NDEF_WriteNDEF(nfc_buffer);
        CloseNDEFSession(NDEF_FILE_ID);
    }
    
    return ATR_SUCCESS;
}