
#ifndef __LIB_APPROACH_TAG_H
#define __LIB_APPROACH_TAG_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "lib_NDEF.h"
  
#define MIMETYPE "application/approach"
  
typedef struct
{
    uint16_t NumberCCByte;
    uint8_t Version;
    uint16_t MaxReadByte;
    uint16_t MaxWriteByte;
    uint8_t TField;
    uint8_t LField;
    uint16_t FileID;
    uint16_t NDEFFileMaxSize;
    uint8_t ReadAccess;
    uint8_t WriteAccess;
}sCCFileInfo;

/**
 * @brief  This structure contains the data of the CC file
 */
static sCCFileInfo CCFileStruct;

typedef struct
{
  bool msgWaiting_state;
}tagState;

static tagState TagState;

typedef enum ApproachTagResult {
  ATR_SUCCESS = 0,//Successfull read ndef
  ATR_MIME_FAIL = 1,//Wrong MIME Type
  ATR_NDEF_FAIL = 2,//NDEF Format error (Mime type size)
  ATR_ALLOC_FAIL = 3//Not enough memory allocated for NDEF
} ApproachTagResult;

// typedef struct {
//   char* record;
//   char* next_record;
//   int size;
// }TagRecord;

typedef struct {
  char* tag_buffer;
  char* record;
  char* payload;
  int tt4_size;
  int record_size;
  int payload_size;
}TagStructure;

uint16_t approachTagInit();
ApproachTagResult approachTagRead(char* nfc_buffer, TagStructure* tagStructure);
ApproachTagResult approachTagWrite(uint8_t* nfc_buffer, uint8_t payload_size, int buffer_size);
void enableTagRF();
void disableTagRF();
void clearMsgWaiting();
bool msgWaiting();

#ifdef __cplusplus
}
#endif

#endif /* __LIB_APPROACH_TAG_H */

